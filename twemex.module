<?php

/**
 * @file
 * Extends Tweme functionality.
 */

/**
 * Implements hook_theme().
 */
function twemex_theme($existing, $type, $theme, $path) {
  return array(
    'disqus_comments' => array(
      'path' => $path . '/templates',
      'template' => 'disqus-comments',
      'variables' => array(
        'disqus_shortname' => NULL,
      ),
    ),
    'twitter_timeline' => array(
      'path' => $path . '/templates',
      'template' => 'twitter-timeline',
      'variables' => array(
        'twitter_username' => NULL,
        'twitter_widget_id' => NULL,
      ),
    ),
    'addthis_layers' => array(
      'path' => $path . '/templates',
      'template' => 'addthis-layers',
      'variables' => array(
        'addthis_profile_id' => NULL,
        'addthis_follow_services' => NULL,
      ),
    ),
    'yandex_metrica' => array(
      'path' => $path . '/templates',
      'template' => 'yandex-metrica',
      'variables' => array(
        'yandex_metrica_counter_id' => NULL,
      ),
    ),
    'google_analytics' => array(
      'path' => $path . '/templates',
      'template' => 'google-analytics',
      'variables' => array(
        'google_analytics_tracking_id' => NULL,
        'domain' => NULL,
      ),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function twemex_menu() {
  return array(
    'admin/config/user-interface/twemex' => array(
      'title' => 'TwemeX',
      'description' => 'Configure TwemeX settings.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('twemex_admin_settings_form'),
      'access arguments' => array('administer site configuration'),
      'file' => 'twemex.admin.inc',
    ),
  );
}

/**
 * Implements hook_block_info().
 */
function twemex_block_info() {
  return array(
    'scrollspynav' => array(
      'info' => t('ScrollSpy navigation'),
      'cache' => DRUPAL_CACHE_PER_PAGE,
    ),
    'disqus_comments' => array(
      'info' => t('Disqus comments'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ),
    'twitter_timeline' => array(
      'info' => t('Twitter timeline'),
      'cache' => DRUPAL_CACHE_GLOBAL,
    ),
  );
}

/**
 * Implements hook_block_view().
 */
function twemex_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'scrollspynav':
      $markup = _twemex_get_region_markup('content');
      if (empty($markup)) {
        break;
      }
      $anchors = _twemex_extract_anchors($markup);
      if (empty($anchors) || !is_array($anchors)) {
        break;
      }
      
      // Everything is ok -> generate block content:
      $content = '<nav id="scrollspynav"><ul class="nav nav-tabs nav-stacked">';
      foreach ($anchors as $item) {
        $content .= '<li><a href="' . $item['href'] . '"><i class="icon icon-chevron-right pull-right"></i>' . $item['title'] . '</a></li>';
      }
      $content .= '</ul></nav>';
      $block['content'] = $content;
      break;
    case 'disqus_comments':
      if ($shortname = variable_get('twemex_disqus_shortname')) {
        $block['content'] = theme('disqus_comments', array('disqus_shortname' => $shortname));
      }
      break;
    case 'twitter_timeline':
      if (($username = variable_get('twemex_twitter_username')) && ($widget_id = variable_get('twemex_twitter_widget_id'))) {
        $block['content'] = theme('twitter_timeline', array(
          'twitter_username' => $username,
          'twitter_widget_id' => $widget_id,
        ));
      }
      break;
  }
  return $block;
}

/**
 * Implements template_preprocess_html().
 */
function twemex_preprocess_html(&$vars) {
  if (_twemex_is_admin_page()) return;

  $vars['attributes_array']['data-spy'] = 'scroll';
  $vars['attributes_array']['data-target'] = '#scrollspynav';

  // Google Analytics:
  if ($tracking_id = variable_get('twemex_google_analytics_tracking_id')) {
    global $base_url;
    $url_parts = parse_url($base_url);
    $vars['page']['page_bottom']['google_analytics'] = array(
      '#theme' => 'google_analytics',
      '#google_analytics_tracking_id' => $tracking_id,
      '#domain' => $url_parts['host'],
    );
  }
  // Yandex Metrica:
  if ($counter_id = variable_get('twemex_yandex_metrica_counter_id')) {
    $vars['page']['page_bottom']['yandex_metrica'] = array(
      '#theme' => 'yandex_metrica',
      '#yandex_metrica_counter_id' => $counter_id,
    );
  }
  // AddThis:
  if (($profile_id = variable_get('twemex_addthis_profile_id')) && ($services_raw = variable_get('twemex_addthis_follow_services'))) {

    $services = array();
    foreach (_twemex_extract_pairs($services_raw) as $pair) {
      $services[] = array(
        'service' => $pair[0],
        'id' => $pair[1],
      );
    }

    $vars['page']['page_bottom']['addthis_layers'] = array(
      '#theme' => 'addthis_layers',
      '#addthis_profile_id' => $profile_id,
      '#addthis_follow_services' => $services,
    );
  }
}

/**
 * Implements template_process_html().
 */
function twemex_process_html(&$vars) {
  if (_twemex_is_admin_page()) return;

  $vars['attributes'] = drupal_attributes($vars['attributes_array']);
}

/**
 * Implements hook_filter_info().
 */
function twemex_filter_info() {
  return array(
    'twemex' => array(
      'title' => t('TwemeX'),
      'process callback' => '_twemex_filter_process',
    ),
  );
}

/**
 * TwemeX filter process callback.
 */
function _twemex_filter_process($text, $filter, $format) {
  $text = preg_replace_callback(
    '|<p>\[columns\]<\/p>([\s\S]*?)<p>\[\/columns\]<\/p>|',
    '_twemex_filter_process_columns', $text);
  $text = str_replace('<table>', '<table class="table table-bordered">', $text);
  return $text;
}

/**
 * TwemeX filter columns process callback.
 */
function _twemex_filter_process_columns($matches) {
  $cols = explode('<p>[--]</p>', $matches[1]);
  $span = 12 / count($cols);

  $out = '<div class="row-fluid">';
  foreach ($cols as $col) {
    $out .= sprintf('<div class="span%s">%s</div>', $span, $col);
  }
  $out .= '</div>';

  return $out;
}

/**
 * Helper function: returns the markup for a specified region.
 */
function _twemex_get_region_markup($region) {
  $blocks = block_list($region);
  $renderable = _block_get_renderable_array($blocks);
  return drupal_render($renderable);
}

/**
 * Helper function: returns the list of heading anchors in the markup.
 */
function _twemex_extract_anchors($markup) {
  $pattern = "/<h[\d] id=\"([^\"]+)\">([^<]+)</";
  preg_match_all($pattern, $markup, $matches);
  $anchors = array();
  foreach ($matches[1] as $i => $id) {
    $anchors[] = array(
      'href' => '#' . $id,
      'title' => $matches[2][$i],
    );
  }
  return $anchors;
}

/**
 * Helper function: extracts key|value pairs from a given string.
 */
function _twemex_extract_pairs($string) {
  $list = explode("\n", $string);
  $list = array_map('trim', $list);
  $list = array_filter($list, 'strlen');

  $pairs = array();
  foreach ($list as $item) {
    $pairs[] = explode('|', $item);
  }
  return $pairs;
}

/**
 * Helper functions: checks if we on the admin page.
 */
function _twemex_is_admin_page() {
  return path_is_admin(current_path());
}
