(function ($) {

  $(function () {
    if (is_scrollable_enough()) {
      $(window).scroll(update_at4share_opacity);
    }
  })

  function is_scrollable_enough() {
    return $(document).height() > 2 * $(window).height();
  }

  function update_at4share_opacity() {
    var at4share = $('#at4-share');
    if (!at4share) return;

    var min_offset = 0,
      current_offset = $(document).scrollTop(),
      max_offset = $(document).height() - $(window).height();

    var opacity = 0.2 + 0.8 * (current_offset - min_offset) / (max_offset - min_offset);

    $('#at4-share a').css('opacity', opacity);
  }

})(jQuery);
