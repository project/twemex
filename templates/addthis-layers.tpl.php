<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=<?php print $addthis_profile_id ?>"></script>
<script type="text/javascript">
  addthis.layers({
    'theme' : 'transparent',
    'share' : {
      'position' : 'left',
      'numPreferredServices' : 5
    }, 
    'follow' : {
      'services' : <?php print json_encode($addthis_follow_services) ?>
    }   
  });
</script>
