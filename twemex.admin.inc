<?php

function twemex_admin_settings_form() {
  $form = array();

  // Google Analytics:
  $form += _twemex_service_element('Google Analytics', 'google_analytics', 'Tracking ID', 'tracking_id');

  // Yandex Metrica:
  $form += _twemex_service_element('Yandex Metrica', 'yandex_metrica', 'Counter ID', 'counter_id');

  // AddThis:
  $form += _twemex_service_element('AddThis', 'addthis', 'Profile ID', 'profile_id', array(
    'twemex_addthis_follow_services' => array(
      '#type' => 'textarea',
      '#title' => t('Follow services'),
      '#default_value' => variable_get('twemex_addthis_follow_services'),
    ),
  ));

  // Disqus:
  $form += _twemex_service_element('Disqus', 'disqus', 'Shortname', 'shortname');

  // Twitter:
  $form += _twemex_service_element('Twitter', 'twitter', 'Username', 'username', array(
    'twemex_twitter_widget_id' => array(
      '#type' => 'textfield',
      '#title' => t('Widget ID'),
      '#default_value' => variable_get('twemex_twitter_widget_id'),
    ),
  ));

  return system_settings_form($form);
}

function _twemex_service_element($title, $title_, $identifier, $identifier_, $children = array()) {
  $element_id = 'twemex_' . $title_;
  $variable_id = 'twemex_' . $title_ . '_' . $identifier_;

  return array(
    $element_id => array(
      '#type' => 'fieldset',
      '#title' => t($title),
      $variable_id => array(
        '#type' => 'textfield',
        '#title' => t($identifier),
        '#default_value' => variable_get($variable_id),
      ),
    ) + $children,
  );
}
